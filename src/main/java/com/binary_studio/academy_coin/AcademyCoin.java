package com.binary_studio.academy_coin;

import java.util.stream.Stream;

public final class AcademyCoin {

	private AcademyCoin() {
	}

	public static int maxProfit(Stream<Integer> prices) {
		Integer[] p = prices.toArray(Integer[]::new);

		int profit = 0;
		int j = 0;

		for (int i = 1; i < p.length; i++) {
			if (p[i - 1] > p[i]) {
				j = i;
			}

			if (p[i - 1] <= p[i] && (i + 1 == p.length || p[i] > p[i + 1])) {
				profit += (p[i] - p[j]);
			}
		}
		return profit;
	}

}
