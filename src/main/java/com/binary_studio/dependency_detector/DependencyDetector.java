package com.binary_studio.dependency_detector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class DependencyDetector {

	private DependencyDetector() {
	}

	private static List<List<Integer>> dependencies;

	private static boolean[] visited;

	private static boolean[] circle;

	public static boolean canBuild(DependencyList libraries) {

		List<String> libs = libraries.libraries;

		int libSize = libs.size();
		visited = new boolean[libSize];
		circle = new boolean[libSize];
		dependencies = new ArrayList<>(libSize);
		for (int i = 0; i < libSize; i++) {
			dependencies.add(new ArrayList<>());
		}

		libraries.dependencies.stream().map(dep -> Arrays.stream(dep).mapToInt(libs::indexOf).toArray())
				.forEach(dep -> dependencies.get(dep[0]).add(dep[1]));

		for (int i = 0; i < libSize; i++) {
			if (findCycle(i)) {
				return false;
			}
		}

		return true;
	}

	private static boolean findCycle(int i) {
		if (circle[i]) {
			return true;
		}

		if (visited[i]) {
			return false;
		}

		circle[i] = true;
		visited[i] = true;

		for (int node : dependencies.get(i)) {
			if (findCycle(node)) {
				return true;
			}
		}

		circle[i] = false;

		return false;
	}

}
