package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger capAmount;

	private final PositiveInteger capRechRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private PositiveInteger capCurrent;

	private PositiveInteger currentHP;

	private PositiveInteger currentShieldHP;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capAmount,
			PositiveInteger capRechRate, PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capAmount = capAmount;
		this.capRechRate = capRechRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;

		this.currentHP = hullHP;
		this.currentShieldHP = shieldHP;
		this.capCurrent = capAmount;
	}

	@Override
	public void endTurn() {
		int maxAmount = this.capAmount.value();
		int restoreRate = this.capRechRate.value();
		int current = this.capCurrent.value();

		this.capCurrent = PositiveInteger
				.of(Math.min(maxAmount, (int) Math.ceil(current + maxAmount * restoreRate / 100.)));
	}

	@Override
	public void startTurn() {
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int current = this.capCurrent.value();
		int regenUsage = this.attackSubsystem.getCapacitorConsumption().value();

		if (current < regenUsage) {
			return Optional.empty();
		}

		this.capCurrent = PositiveInteger.of(current - regenUsage);

		return Optional.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		int currShield = this.currentShieldHP.value();
		int currHP = this.currentHP.value();

		AttackAction reduced = this.defenciveSubsystem.reduceDamage(attack);

		currShield -= reduced.damage.value();

		Optional<AttackResult> res = Optional.empty();

		if (currShield <= 0) {
			currHP += currShield;
			currShield = 0;

			if (currHP <= 0) {
				currHP = 0;
				res = Optional.of(new AttackResult.Destroyed());
			}
		}

		this.currentShieldHP = PositiveInteger.of(currShield);
		this.currentHP = PositiveInteger.of(currHP);

		return res.orElse(new AttackResult.DamageRecived(attack.weapon, reduced.damage, this));
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int current = this.capCurrent.value();
		int regenUsage = this.defenciveSubsystem.getCapacitorConsumption().value();

		if (current < regenUsage) {
			return Optional.empty();
		}

		this.capCurrent = PositiveInteger.of(current - regenUsage);

		RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
		int currHP = this.currentHP.value();
		int currShield = this.currentShieldHP.value();
		int newHP = Math.min(this.hullHP.value(), currHP + regenerateAction.hullHPRegenerated.value());
		int newShieldHP = Math.min(this.shieldHP.value(), currShield + regenerateAction.shieldHPRegenerated.value());

		this.currentHP = PositiveInteger.of(newHP);
		this.currentShieldHP = PositiveInteger.of(newShieldHP);
		return Optional.of(
				new RegenerateAction(PositiveInteger.of(newShieldHP - currShield), PositiveInteger.of(newHP - currHP)));
	}

}
