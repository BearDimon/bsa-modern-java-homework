package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger powergridOut;

	private final PositiveInteger capAmount;

	private final PositiveInteger capRechRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOut,
			PositiveInteger capAmount, PositiveInteger capRechRate, PositiveInteger speed, PositiveInteger size)
			throws IllegalArgumentException {
		if (name == null || name.strip().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOut = powergridOut;
		this.capAmount = capAmount;
		this.capRechRate = capRechRate;
		this.speed = speed;
		this.size = size;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
			return;
		}

		int total = subsystem.getPowerGridConsumption().value();

		if (this.defenciveSubsystem != null) {
			total += this.defenciveSubsystem.getPowerGridConsumption().value();
		}

		int chipOut = this.powergridOut.value();
		if (total > chipOut) {
			throw new InsufficientPowergridException(total - chipOut);
		}

		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
			return;
		}

		int total = subsystem.getPowerGridConsumption().value();

		if (this.attackSubsystem != null) {
			total += this.attackSubsystem.getPowerGridConsumption().value();
		}

		int chipOut = this.powergridOut.value();
		if (total > chipOut) {
			throw new InsufficientPowergridException(total - chipOut);
		}

		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else {
			return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capAmount, this.capRechRate,
					this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
		}
	}

}
