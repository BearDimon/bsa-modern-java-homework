package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private static final int MAX_PERCENTAGE_REDUCTION = 95;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	private final String name;

	private final PositiveInteger powergridCons;

	private final PositiveInteger capCons;

	private final PositiveInteger impactRedPer;

	private final PositiveInteger shieldReg;

	private final PositiveInteger hullReg;

	public DefenciveSubsystemImpl(String name, PositiveInteger powergridCons, PositiveInteger capCons,
			PositiveInteger impactRedPer, PositiveInteger shieldReg, PositiveInteger hullReg) {
		if (name == null || name.strip().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.powergridCons = powergridCons;
		this.capCons = capCons;
		this.impactRedPer = impactRedPer;
		this.shieldReg = shieldReg;
		this.hullReg = hullReg;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridCons;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capCons;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int damage = (int) Math.ceil(incomingDamage.damage.value()
				* (100 - Math.min(MAX_PERCENTAGE_REDUCTION, this.impactRedPer.value())) / 100f);
		damage = damage == 0 ? 1 : damage;
		return new AttackAction(PositiveInteger.of(damage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldReg, this.hullReg);
	}

}
