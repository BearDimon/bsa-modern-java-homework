package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirements,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		return new AttackSubsystemImpl(name, powergridRequirements, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	private final String name;

	private final PositiveInteger powergridReq;

	private final PositiveInteger capCons;

	private final PositiveInteger optSpeed;

	private final PositiveInteger optSize;

	private final PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger powergridReq, PositiveInteger capCons,
			PositiveInteger optSpeed, PositiveInteger optSize, PositiveInteger baseDamage)
			throws IllegalArgumentException {
		if (name == null || name.strip().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name.strip();
		this.powergridReq = powergridReq;
		this.capCons = capCons;
		this.optSpeed = optSpeed;
		this.optSize = optSize;
		this.baseDamage = baseDamage;

	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powergridReq;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capCons;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		int tgtSize = target.getSize().value();
		int tgtSpeed = target.getCurrentSpeed().value();

		int size = this.optSize.value();

		double sizeReductionModifier = tgtSize >= size ? 1 : tgtSize * 1. / size;

		int speed = this.optSpeed.value();

		double speedReductionModifier = tgtSpeed <= speed ? 1 : speed * 1. / (2 * tgtSpeed);

		double res = this.baseDamage.value() * Math.min(speedReductionModifier, sizeReductionModifier);

		return PositiveInteger.of((int) Math.ceil(res));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
